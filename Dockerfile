FROM docker-jive
env DOCYUMPACKAGES "jive_pdf2swf*"
RUN echo "10.114.138.51 aptplaton.si.francetelecom.fr" >> /etc/hosts &&\
    yum -y install $DOCYUMPACKAGES --nogpgcheck --disablerepo=rhel* &&\
    yum clean all &&\
    rm -fr /var/cache/yum &&\
    ln -sf /etc/fonts /usr/local/etc/fonts

COPY *.sh /
COPY conf/*.conf /etc/
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
