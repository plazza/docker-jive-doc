#!/bin/sh

if [[ $DOC_ENABLED  == true ]]
then

if [ -z $DOC_XMX ]; then
  DOC_XMX=512
fi

if [ -z $DOC_XMS ]; then
  DOC_XMS=$DOC_XMX
fi
#### PARAMETRE LIMITS#####
echo "#Paramètres appliqués pour JIVE" >> /etc/security/limits.conf
echo "jive    soft    nofile  100000" >> /etc/security/limits.conf
echo "jive    hard    nofile  200000" >> /etc/security/limits.conf
##########################
ln -sf /etc/fonts /usr/local/etc/fonts
runuser -l jive -c 'jive enable docconverter'
runuser -l jive -c "jive set docconverter.custom_jvm_args  \"-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8999 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=$HOSTNAME\""
runuser -l jive -c "jive set docconverter.jvm_heap_min $DOC_XMS"
runuser -l jive -c "jive set docconverter.jvm_heap_max $DOC_XMX"

jivestatus=1
while [ $jivestatus -eq 1 ]
do
        runuser -l jive -c 'jive status -v | grep 8820'
        if [ $? -eq 0 ]
        then
                jivestatus=0
        else
                runuser -l jive -c 'jive stop'
                sleep 20
        fi
done
fi
